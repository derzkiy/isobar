<?php

namespace IsobarTestBundle\EventListener;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use IsobarTestBundle\Entity\Post;
use IsobarTestBundle\ImageUploader;

class ImageUploadListener
{
    private $uploader;

    public function __construct(ImageUploader $uploader)
    {
        $this->uploader = $uploader;
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        $this->uploadImage($entity);
    }

    public function preUpdate(PreUpdateEventArgs $args)
    {
        $entity = $args->getEntity();

        $this->uploadImage($entity);
    }

    public function preRemove(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $image = $entity->getImage();
        
        $this->uploader->remove($image);

    }

    private function uploadImage($entity)
    {
        if (!$entity instanceof Post) {
            return;
        }

        $image = $entity->getImage();

        if (!$image instanceof UploadedFile) {
            return;
        }

        $imageName = $this->uploader->upload($image);
        $entity->setImage($imageName);
    }
}
