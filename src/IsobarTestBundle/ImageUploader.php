<?php

namespace IsobarTestBundle;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImageUploader
{
    private $targetDir;

    public function __construct($targetDir)
    {
        $this->targetDir = $targetDir;
    }

    public function upload(UploadedFile $image)
    {
        $imageName = md5(uniqid()).'.'.$image->guessExtension();

        $image->move($this->targetDir, $imageName);

        return $imageName;
    }

    public function remove($image)
    {
        if ($path = realpath($this->targetDir.'/'.$image)) {
            unlink($path);
        }
    }
}
